package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.ISessionRepository;
import com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO;
import org.jetbrains.annotations.Nullable;

public class SessionRepository implements ISessionRepository {

    @Nullable
    private SessionDTO session;

    public @Nullable SessionDTO getSession() {
        return session;
    }

    public void setSession(@Nullable final SessionDTO session) {
        this.session = session;
    }

}
