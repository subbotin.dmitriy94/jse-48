package com.tsconsulting.dsubbotin.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

import java.util.Scanner;

@UtilityClass
public final class TerminalUtil {

    @NotNull
    private static final Scanner SCANNER = new Scanner(System.in);

    @NotNull
    public static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    public static Integer nextNumber() {
        try {
            return Integer.parseInt(SCANNER.nextLine());
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Incorrect number entered!");
        }
    }

    public static void printMessage(@NotNull final String message) {
        System.out.println(message);
    }

}
