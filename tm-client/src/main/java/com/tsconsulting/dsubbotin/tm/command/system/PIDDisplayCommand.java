package com.tsconsulting.dsubbotin.tm.command.system;

import com.tsconsulting.dsubbotin.tm.command.AbstractSystemCommand;
import com.tsconsulting.dsubbotin.tm.util.SystemUtil;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class PIDDisplayCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String name() {
        return "pid";
    }

    @Override
    @NotNull
    public String arg() {
        return "-p";
    }

    @Override
    @NotNull
    public String description() {
        return "Display Process IDentifier.";
    }

    @Override
    public void execute() {
        TerminalUtil.printMessage(Long.toString(SystemUtil.getPID()));
    }

}
