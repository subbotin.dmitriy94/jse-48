package com.tsconsulting.dsubbotin.tm.util;

import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateAdapter extends XmlAdapter<String, Date> {

    @NotNull
    private final static String PATTERN_DATE_FORMAT = "dd.MM.yyyy HH:mm:ss";

    @NotNull
    private final static SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat(PATTERN_DATE_FORMAT);

    @NotNull
    public static SimpleDateFormat getSimpleDateFormat() {
        return SIMPLE_DATE_FORMAT;
    }

    @Override
    @NotNull
    public String marshal(Date v) {
        return getSimpleDateFormat().format(v);
    }

    @Override
    @NotNull
    public Date unmarshal(String v) throws ParseException {
        return getSimpleDateFormat().parse(v);
    }

}