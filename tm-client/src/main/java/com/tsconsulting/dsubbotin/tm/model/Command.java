package com.tsconsulting.dsubbotin.tm.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

@Getter
@RequiredArgsConstructor
public final class Command {

    private final String name;

    private final String argument;

    private final String description;

    @Override
    public String toString() {
        @NotNull final StringBuilder sbCommandToString = new StringBuilder();
        if (this.name != null && !this.name.isEmpty()) sbCommandToString.append(this.name).append(" ");
        if (this.argument != null && !this.argument.isEmpty())
            sbCommandToString.append(String.format("(%s) ", this.argument));
        if (this.description != null && !this.description.isEmpty())
            sbCommandToString.append("- ").append(this.description);
        return sbCommandToString.toString();
    }

}
