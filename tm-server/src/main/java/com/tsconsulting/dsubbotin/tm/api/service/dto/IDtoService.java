package com.tsconsulting.dsubbotin.tm.api.service.dto;

import com.tsconsulting.dsubbotin.tm.dto.AbstractEntityDTO;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface IDtoService<E extends AbstractEntityDTO> {

    @NotNull
    List<E> findAll() throws AbstractException;

    @NotNull
    E findById(@NotNull String id) throws AbstractException;

    @NotNull
    E findByIndex(int index) throws AbstractException;

    void clear() throws AbstractException;

    void addAll(@NotNull List<E> entities) throws AbstractException;

}