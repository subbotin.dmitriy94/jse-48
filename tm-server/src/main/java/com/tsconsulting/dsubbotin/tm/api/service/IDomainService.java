package com.tsconsulting.dsubbotin.tm.api.service;

public interface IDomainService {

    void dataLoadJsonJaxb() throws Exception;

    void dataSaveJsonJaxb() throws Exception;

    void dataLoadXmlJaxb() throws Exception;

    void dataSaveXmlJaxb() throws Exception;

    void dataLoadJsonFasterXml() throws Exception;

    void dataSaveJsonFasterXml() throws Exception;

    void dataLoadXmlFasterXml() throws Exception;

    void dataSaveXmlFasterXml() throws Exception;

    void loadBackup() throws Exception;

    void saveBackup() throws Exception;

    void loadBinary() throws Exception;

    void saveBinary() throws Exception;

    void loadBase64() throws Exception;

    void saveBase64() throws Exception;

}

