package com.tsconsulting.dsubbotin.tm.endpoint;

import com.tsconsulting.dsubbotin.tm.api.endpoint.IProjectTaskEndpoint;
import com.tsconsulting.dsubbotin.tm.api.service.IServiceLocator;
import com.tsconsulting.dsubbotin.tm.dto.SessionDTO;
import com.tsconsulting.dsubbotin.tm.dto.TaskDTO;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectTaskEndpoint extends AbstractEndpoint implements IProjectTaskEndpoint {

    public ProjectTaskEndpoint() {
        super(null);
    }

    public ProjectTaskEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public void bindTaskToProject(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "projectId") String projectId,
            @NotNull @WebParam(name = "taskId") String taskId
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
    }

    @Override
    @WebMethod
    public void unbindTaskFromProject(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "projectId") String projectId,
            @NotNull @WebParam(name = "taskId") String taskId
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
    }

    @Override
    @NotNull
    @WebMethod
    public List<TaskDTO> findAllTasksByProjectId(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "id") String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        return serviceLocator.getProjectTaskService().findAllTasksByProjectId(userId, id);
    }

    @Override
    @WebMethod
    public void removeProjectById(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "id") String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getProjectTaskService().removeProjectById(userId, id);
    }

    @Override
    @WebMethod
    public void removeProjectByIndex(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "index") int index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getProjectTaskService().removeProjectByIndex(userId, index);
    }

    @Override
    @WebMethod
    public void removeProjectByName(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "name") String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getProjectTaskService().removeProjectByName(userId, name);
    }

    @Override
    @WebMethod
    public void removeAllProject(
            @Nullable @WebParam(name = "session") SessionDTO session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getProjectTaskService().removeAllProject(userId);
    }

}
