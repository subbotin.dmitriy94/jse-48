package com.tsconsulting.dsubbotin.tm.api.repository.dto;

import com.tsconsulting.dsubbotin.tm.dto.AbstractOwnerEntityDTO;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface IOwnerDtoRepository<E extends AbstractOwnerEntityDTO> extends IDtoRepository<E> {

    @NotNull
    List<E> findAll(@NotNull String userId);

    @NotNull
    E findById(@NotNull String userId, @NotNull String id) throws AbstractException;

    @NotNull
    E findByIndex(@NotNull String userId, int index) throws AbstractException;

    @NotNull
    E findByName(@NotNull String userId, @NotNull String name) throws AbstractException;

    void create(@NotNull String userId, @NotNull E entity);

    void clear(@NotNull String userId);

}