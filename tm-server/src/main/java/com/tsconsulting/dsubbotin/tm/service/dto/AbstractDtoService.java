package com.tsconsulting.dsubbotin.tm.service.dto;

import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.ILogService;
import com.tsconsulting.dsubbotin.tm.api.service.dto.IDtoService;
import com.tsconsulting.dsubbotin.tm.dto.AbstractEntityDTO;
import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;

@AllArgsConstructor
public abstract class AbstractDtoService<E extends AbstractEntityDTO> implements IDtoService<E> {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    protected final ILogService logService;

}
