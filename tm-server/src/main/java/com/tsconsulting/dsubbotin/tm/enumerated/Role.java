package com.tsconsulting.dsubbotin.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum Role {

    ADMIN("Admin"),
    USER("User");

    @NotNull
    private final String displayName;

    Role(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}
