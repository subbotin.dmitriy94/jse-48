package com.tsconsulting.dsubbotin.tm.repository.dto;

import com.tsconsulting.dsubbotin.tm.api.repository.dto.ITaskDtoRepository;
import com.tsconsulting.dsubbotin.tm.dto.TaskDTO;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.TaskNotFoundException;
import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import java.util.List;

public class TaskDtoRepository extends AbstractOwnerDtoRepository<TaskDTO> implements ITaskDtoRepository {

    public TaskDtoRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll() {
        return entityManager
                .createQuery("FROM TaskDTO", TaskDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@NotNull final String userId) {
        return entityManager
                .createQuery("FROM TaskDTO WHERE userId = :userId", TaskDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public TaskDTO findById(@NotNull final String id) throws AbstractException {
        return entityManager
                .createQuery("FROM TaskDTO WHERE id = :id", TaskDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("id", id)
                .getResultStream()
                .findFirst()
                .orElseThrow(TaskNotFoundException::new);
    }

    @NotNull
    @Override
    public TaskDTO findById(@NotNull final String userId, @NotNull final String id) throws AbstractException {
        return entityManager
                .createQuery("FROM TaskDTO WHERE userId = :userId AND id = :id", TaskDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getResultStream()
                .findFirst()
                .orElseThrow(TaskNotFoundException::new);
    }

    @NotNull
    @Override
    public TaskDTO findByIndex(final int index) throws AbstractException {
        return entityManager
                .createQuery("FROM TaskDTO", TaskDTO.class)
                .setMaxResults(1)
                .setFirstResult(index)
                .getResultStream()
                .findFirst()
                .orElseThrow(TaskNotFoundException::new);
    }

    @NotNull
    @Override
    public TaskDTO findByIndex(@NotNull final String userId, final int index) throws AbstractException {
        return entityManager
                .createQuery("FROM TaskDTO WHERE userId = :userId", TaskDTO.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .setFirstResult(index)
                .getResultStream()
                .findFirst()
                .orElseThrow(TaskNotFoundException::new);
    }

    @NotNull
    @Override
    public TaskDTO findByName(@NotNull final String userId, @NotNull final String name) throws AbstractException {
        return entityManager
                .createQuery("FROM TaskDTO WHERE userId = :userId AND name = :name", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .getResultStream()
                .findFirst()
                .orElseThrow(TaskNotFoundException::new);
    }

    @Override
    public void clear(@NotNull final String userId) {
        entityManager
                .createQuery("DELETE TaskDTO WHERE userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        return entityManager
                .createQuery("FROM TaskDTO WHERE projectId = :projectId", TaskDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("projectId", id)
                .getResultList();
    }

    @Override
    public void removeAllTaskByProjectId(@NotNull final String userId, @NotNull final String id) {
        entityManager
                .createQuery("UPDATE TaskDTO SET projectId = NULL WHERE userId = :userId AND projectId = :projectId")
                .setParameter("userId", userId)
                .setParameter("projectId", id)
                .executeUpdate();
    }

    @Override
    public boolean existById(@NotNull String userId, @NotNull String id) {
        return entityManager
                .createQuery("FROM TaskDTO WHERE userId = :userId AND id = :id", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getResultStream()
                .findFirst()
                .isPresent();
    }

}
