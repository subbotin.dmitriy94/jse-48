package com.tsconsulting.dsubbotin.tm.api.repository.entity;

import com.tsconsulting.dsubbotin.tm.entity.Task;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface ITaskRepository extends IOwnerRepository<Task> {

    boolean existById(@NotNull String userId, @NotNull String taskId);

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String id);

    void removeAllTaskByProjectId(@NotNull String userId, @NotNull String id);

}
