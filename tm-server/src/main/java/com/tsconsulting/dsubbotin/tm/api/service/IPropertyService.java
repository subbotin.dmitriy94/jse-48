package com.tsconsulting.dsubbotin.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getDeveloperName();

    @NotNull
    String getDeveloperEmail();

    int getPasswordIteration();

    @NotNull
    String getPasswordSecret();

    int getSignatureIteration();

    @NotNull
    String getSignatureSecret();

    @NotNull
    String getServerHost();

    @NotNull
    String getServerPort();

    @NotNull
    String getJdbcDriver();

    @NotNull
    String getJdbcUrl();

    @NotNull
    String getJdbcLogin();

    @NotNull
    String getJdbcPassword();

    @NotNull
    String getHibernateDialect();

    @NotNull
    String getHibernateShowSql();

    @NotNull
    String getHibernateFormatSql();

    @NotNull
    String getHibernateHbm2Ddl();

    @NotNull
    String getHibernateCacheUseSecondLevelCache();

    @NotNull
    String getHibernateCacheUseQueryCache();

    @NotNull
    String getHibernateCacheUseMinimalPuts();

    @NotNull
    String getHibernateCacheHazelcastUseLiteMember();

    @NotNull
    String getHibernateCacheRegionPrefix();

    @NotNull
    String getHibernateCacheProviderConfigurationFileResourcePath();

    @NotNull
    String getHibernateCacheRegionFactoryClass();

}
