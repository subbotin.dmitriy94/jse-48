package com.tsconsulting.dsubbotin.tm.api.endpoint;

import com.tsconsulting.dsubbotin.tm.dto.SessionDTO;
import com.tsconsulting.dsubbotin.tm.dto.UserDTO;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IAdminUserEndpoint {

    @NotNull
    @WebMethod
    UserDTO createUser(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "login") String login,
            @NotNull @WebParam(name = "password") String password,
            @NotNull @WebParam(name = "email") String email
    ) throws AbstractException;

    @NotNull
    @WebMethod
    List<UserDTO> findAllUser(
            @Nullable @WebParam(name = "session") SessionDTO session
    ) throws AbstractException;

    @NotNull
    @WebMethod
    UserDTO findByIdUser(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "id") String id
    ) throws AbstractException;

    @NotNull
    @WebMethod
    UserDTO findByLoginUser(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "login") String login
    ) throws AbstractException;

    @NotNull
    @WebMethod
    UserDTO findByIndexUser(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "index") int index
    ) throws AbstractException;

    @WebMethod
    void removeByLoginUser(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "login") String login
    ) throws AbstractException;

    @WebMethod
    void removeByIdUser(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "id") String id
    ) throws AbstractException;

    @WebMethod
    void removeByIndexUser(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "index") int index
    ) throws AbstractException;

    @WebMethod
    void clearUser(
            @Nullable @WebParam(name = "session") SessionDTO session
    ) throws AbstractException;

    @WebMethod
    void setRoleUser(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "id") String id,
            @NotNull @WebParam(name = "role") Role role
    ) throws AbstractException;

    @WebMethod
    void lockByLoginUser(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "login") String login
    ) throws AbstractException;

    @WebMethod
    void unlockByLoginUser(
            @Nullable @WebParam(name = "session") SessionDTO session,
            @NotNull @WebParam(name = "login") String login
    ) throws AbstractException;

}
